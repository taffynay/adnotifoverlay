Instructions:
1. Download and install Streamer.bot from https://www.streamer.bot
2. In Streamer.bot, navigate to Platforms > Twitch > Accounts and sign
in with your Twitch Account
3. In Streamer.bot, navigate to Servers/Clients> Websocket Server
4. Check "Auto Start" and click "Start Server". Leave the "Address" as
127.0.0.1 and the "Port" as 8080.
5. If the widget changed from "Connecting..." to "Connected", you have
successfully set up the widget
6. Add the widget to OBS/SLOBS just like any other widget and enjoy!

Note: You must have Streamer.bot running for the widget to work.
